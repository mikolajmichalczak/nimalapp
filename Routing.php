<?php

require_once 'src/controllers/DefaultController.php';
require_once 'src/controllers/SecurityController.php';
require_once 'src/controllers/AnimalController.php';

class Router{

    public static $routes;

    public static function get($url, $view){
        self::$routes[$url] = $view;
    }

    public static function post($url, $view){
        self::$routes[$url] = $view;
    }

    public static function run($url){

        $urlParts = explode("/", parse_url(trim($_SERVER['REQUEST_URI'], '/'), PHP_URL_PATH));
        $action = $urlParts[0];

        $url_components = parse_url($url);
        parse_str($url_components['query'], $params);

        if(!array_key_exists($action, self::$routes)){
            die("Wrong url!");
        }

        $controller = self::$routes[$action];
        $object = new $controller;
        $action = $action ?: 'index';

        if(!empty($params)){
            $object -> $action($params);
        } else {
            $id = $urlParts[1] ?: "";
            $object -> $action($id);
        }

    }

}
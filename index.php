<?php

require 'Routing.php';

$path = $_SERVER['REQUEST_URI'];

Router::get('', 'DefaultController');
Router::get('home', 'AnimalController');
Router::get('animals', 'AnimalController');
Router::post('login', 'SecurityController');
Router::post('addAnimal', 'AnimalController');
Router::get('createAnimal', 'AnimalController');
Router::get('deleteAnimal', 'AnimalController');
Router::post('register', 'SecurityController');
Router::post('search', 'AnimalController');
Router::get('previewAnimal', 'AnimalController');
Router::get('showOnMap', 'AnimalController');
Router::get('getUserAnimals', 'AnimalController');
Router::get('logout', 'SecurityController');

Router::run($path);

<!DOCTYPE html>
<?php

require_once __DIR__ . '/../../session.php';
handleSession();

?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/animals.css">
    <title>Animal</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    <script src="https://kit.fontawesome.com/d52662d28b.js" crossorigin="anonymous"></script>
</head>

<body>
<div class="home-base-container">
    <header id="main-header">
        <div id="search-bar-container" style="flex-grow: 1; flex-basis: 0;"></div>
        <div id="logo-container" style="flex-grow: 1; flex-basis: 0; justify-content: center">
            <img src="public/img/nimalLogo.svg">
        </div>
        <div class="logout-container" style="flex-grow: 1; flex-basis: 0;">
            <div id="animals-markers-switch-container" style="visibility: hidden">
                <p>Indicative markers visibility: </p>
                <label class="markers-switch">
                    <input type="checkbox" checked>
                    <span class="slider round"></span>
                </label>
            </div>
            <i id="logout-button" class="fas fa-sign-out-alt fa-lg"></i>
        </div>
    </header>
    <form id="animal-preview-container" action="addAnimal" method="POST" ENCTYPE="multipart/form-data">
        <div class="animal-photo-container">
            <?php if (isset($animal)) { ?>
                <img src="public/uploads/<?= $animal->getImage(); ?>">
            <?php } else { ?>
                <div id="drop-container">
                    <i class="fas fa-cloud-upload-alt fa-4x"></i>
                    <p>Drag images here, or...</p>
                    <label id="file-input-label">
                        <p>Select image from your computer</p>
                        <input id="file-input" type="file" name="file"/><br/>
                    </label>
                </div>
            <?php } ?>
        </div>
        <div class="animal-options-container" style="padding-top: 0;">
            <div class="animal-options">
                <div class="circles">
                    <button id="accept-button" class="options-circle" type="submit"><i class="fas fa-check fa-lg"></i></button>
                    <div id="decline-button" class="options-circle">
                        <i class="fas fa-times fa-lg"></i>
                    </div>
                </div>
                <div class="animal-form-inputs-container">
                    <div class="messages">
                        <?php
                        if (isset($messages)) {
                            foreach ($messages as $message) {
                                echo "$message \r\n";
                            }
                        }
                        ?>
                    </div>
                    <div>
                        <p class="animal-info-title">Name</p>
                        <input name="name" type="text">
                    </div>
                    <div>
                        <p class="animal-info-title">Location</p>
                        <input name="location" type="text">
                    </div>
                    <div>
                        <p class="animal-info-title">Coordinates</p>
                        <p class="animal-info"><?= $latitude ?> / <?= $longitude ?></p>
                        <input style="display: none" name="latitude" type="number" value="<?=$latitude?>">
                        <input style="display: none" name="longitude" type="number" value="<?=$longitude?>">
                        <input style="display: none" name="exact" type="text" value="<?=$isExactLocation?>">
                    </div>
                    <div>
                        <p class="animal-info-title">Date</p>
                        <input style="text-align: center; padding-left: 2.4em; width: 94%" name="date" type="date">
                    </div>
                    <div>
                        <p class="animal-info-title">Description</p>
                        <textarea name="description" rows=5></textarea>
                    </div>
                </div>
            </div>
        </div>
    </form>

</div>
<script>

    let myScript = document.body.appendChild(document.createElement("script"));
    myScript.src = "public/js/add-animal.js";

</script>
</body>
</html>
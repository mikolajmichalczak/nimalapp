<!DOCTYPE html>
<?php

    require_once  __DIR__.'/../../session.php';
    handleSession();

?>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://kit.fontawesome.com/d52662d28b.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="public/css/style.css">
        <title>Home</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v2.6.0/mapbox-gl.js'></script>
        <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v2.6.0/mapbox-gl.css' rel='stylesheet' />

        <script type="text/javascript" src="public/js/utils.js" defer></script>
        <script type="text/javascript" src="public/js/map.js" defer></script>
        <script type="text/javascript" src="public/js/animal.js" defer></script>
    </head>

    <body>

    <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.7.2/mapbox-gl-geocoder.min.js"></script>
    <link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.7.2/mapbox-gl-geocoder.css" type="text/css">

    <style>
        .geocoder {
            width: 100%;
            height: 78%;
            background: #FFFFFFE6 0 0 no-repeat padding-box;
            border-radius: 30px;
            display: flex;
            align-items: center;
            padding-left:1em;
        }
        .mapboxgl-ctrl-geocoder {
            min-width: 100%;
            background: none;
            box-shadow: none;
        }
        .mapboxgl-ctrl-geocoder input[type=text] {
            outline: 0;
        }
        
    </style>

        <div class="home-base-container">
            <header id="main-header">
                <div id="logo-container">
                    <img src="public/img/nimalLogo.svg">
                </div>
                <div id="search-bar-container">
                    <div id="geocoder" class="geocoder"></div>
                </div>
                <div class="logout-container">
                    <div class="markers-switch-container">
                        <p>Indicative markers visibility: </p>
                        <label class="markers-switch">
                            <input type="checkbox" checked>
                            <span class="slider round"></span>
                        </label>
                    </div>
                    <i id="logout-button" class="fas fa-sign-out-alt fa-lg"></i>
                </div>
            </header>
            <div id="map"></div>
            <div id="animals-list-button" class="options-circle">
                <i class="fas fa-list-ul fa-lg"></i>
            </div>

        </div>

    <div hidden id="spinner"></div>

    </body>

</html>
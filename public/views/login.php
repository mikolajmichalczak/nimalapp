<!DOCTYPE html>
<?php

    session_start();
    if (isset($_SESSION['user_id'])) {
        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/home");
    }

?>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="public/css/style.css">
        <title>Login</title>
    </head>

    <body>

        <div class="login-base-container">
            <img class="logo" src="public/img/nimalLogo.svg">
            <div class="login-container">
                <div class="login-label">
                    <h1>Login</h1>
                </div>
                <div class="messages">
                    <?php
                    if(isset($messages)){
                        foreach($messages as $message) {
                            echo $message;
                        }
                    }
                    ?>
                </div>
                <form class="login-form" action="login" method="POST">
                    <input name="email" placeholder="Email" type="email">
                    <input name="password" placeholder="Password" type="password">
                    <button id="login-button" class="submit-button" type="submit">LOGIN</button>
                </form>
                <p>Not registered yet? <a style="text-decoration: none;" href="/register">Create an Account</a></p>
            </div>
        </div>

    </body>

</html>
<!DOCTYPE html>
<?php

    require_once  __DIR__.'/../../session.php';
    handleSession();

?>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">


        <link rel="stylesheet" type="text/css" href="public/css/style.css">
        <link rel="stylesheet" type="text/css" href="public/css/animals.css">
        <title>Animal</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

        <script src="https://kit.fontawesome.com/d52662d28b.js" crossorigin="anonymous"></script>
    </head>

    <body>

    <div class="home-base-container">
        <header id="main-header">
            <div id="search-bar-container" style="flex-grow: 1; flex-basis: 0;"></div>
            <div id="logo-container" style="flex-grow: 1; flex-basis: 0; justify-content: center">
                <img src="public/img/nimalLogo.svg">
            </div>
            <div class="logout-container" style="flex-grow: 1; flex-basis: 0;">
                <div id="animals-markers-switch-container" style="visibility: hidden" >
                    <p>Indicative markers visibility: </p>
                    <label class="markers-switch">
                        <input type="checkbox" checked>
                        <span class="slider round"></span>
                    </label>
                </div>
                <i id="logout-button" class="fas fa-sign-out-alt fa-lg"></i>
            </div>
        </header>
        <section id="animal-preview-container">
            <div id="close-preview-button" class="options-circle">
                <i class="fas fa-times fa-2x"></i>
            </div>
            <div class="animal-photo-container">
                <img src="public/uploads/<?= $animal->getImage(); ?>">
            </div>
            <div class="animal-options-container">
                <div class="animal-options">
                    <div class="circles">
                        <div id="show-on-map-button" class="options-circle">
                            <i class="fas fa-map-marked-alt fa-lg"></i>
                            <div id="<?= $animal->getId(); ?>" style="display: none"></div>
                        </div>
                        <div id="delete-button" class="options-circle">
                            <i class="fas fa-trash fa-lg"></i>
                        </div>
                    </div>
                    <div class="animal-info-container">
                        <div>
                            <p class="animal-info-title">Name</p>
                            <p class="animal-info"><?= $animal->getName(); ?></p>
                        </div>
                        <div>
                            <p class="animal-info-title">Location</p>
                            <p class="animal-info"><?= $animal->getLocationName(); ?></p>
                        </div>
                        <div>
                            <p class="animal-info-title">Coordinates</p>
                            <p class="animal-info"><?= $animal->getLatitude(); ?> / <?= $animal->getLongitude(); ?></p>
                        </div>
                        <div>
                            <p class="animal-info-title">Date</p>
                            <p class="animal-info"><?= date('M d, Y ', strtotime($animal->getCreatedAt())); ?></p>
                        </div>
                        <div>
                            <p class="animal-info-title">Description</p>
                            <p class="animal-info"><?= $animal->getDescription(); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <div hidden id="spinner"></div>

    <script>

        let myscript = document.body.appendChild(document.createElement("script"));
        myscript.src = "public/js/preview-animal.js";

    </script>
    </body>

</html>
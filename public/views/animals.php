<!DOCTYPE html>
<?php

    require_once  __DIR__.'/../../session.php';
    handleSession();

?>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <script src="https://kit.fontawesome.com/d52662d28b.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="./public/js/utils.js" defer></script>
        <script type="text/javascript" src="./public/js/search.js" defer></script>
        <script type="text/javascript" src="public/js/animal.js" defer></script>

        <link rel="stylesheet" type="text/css" href="public/css/style.css">
        <link rel="stylesheet" type="text/css" href="public/css/animals.css">
        <title>Animals</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    </head>

    <body>

        <div class="home-base-container">
            <header id="main-header" style="height: 9.35%">
                <div id="search-bar-container" style="flex-grow: 1; flex-basis: 0;"></div>
                <div id="logo-container" style="flex-grow: 1; flex-basis: 0; justify-content: center">
                    <img src="public/img/nimalLogo.svg">
                </div>
                <div class="logout-container" style="flex-grow: 1; flex-basis: 0;">
                    <div id="animals-markers-switch-container" class="markers-switch-container" >
                        <p>Indicative markers visibility: </p>
                        <label class="markers-switch">
                            <input type="checkbox" checked>
                            <span class="slider round"></span>
                        </label>
                    </div>
                    <i id="logout-button" class="fas fa-sign-out-alt fa-lg"></i>
                </div>
            </header>
            <header id="animals-filters-header">
                <div id="sort-container">
                    <p>Sort by: </p>
                    <div class="sort-options-container">
                    <div id="date-sort" class="animals-sort-option">Date</div>
                    <div id="accuracy-sort" class="animals-sort-option">Marker accuracy</div>
                    <div id="name-sort" class="animals-sort-option">Name</div>
                    </div>
                </div>
                <div id="animals-search-bar-container" style="flex-grow: 1; flex-basis: 0;">
                    <div class="animals-search-bar">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        <input type="text" placeholder="Search for animals">
                    </div>
                </div>
                <div class="animals-logout-container">
                    <div id="filter-markers-switch-container" class="markers-switch-container" style="margin-right: 0">
                        <p style="color: #26250F;">Indicative markers visibility: </p>
                        <label class="markers-switch">
                            <input type="checkbox" checked>
                            <span class="slider round"></span>
                        </label>
                    </div>
                </div>
            </header>
            <section class="animals" >
                <?php foreach($animals as $animal): ?>
                    <div  id="<?= $animal->getId(); ?>" class="animal-item">
                        <img id="animal-image" src="public/uploads/<?= $animal->getImage(); ?>">
                        <div class="animal-description-preview" <?php if ($animal -> getIsExactLocation() != "true" ) { ?> style="background-color: #BFD3D3; color: #26250F;" <?php } ?>><p><?= $animal->getDescription(); ?></p></div>
                        <div class="animal-item-content" <?php if ($animal -> getIsExactLocation() != "true" ) { ?> style="background-color: #BFD3D3; color: #26250F;" <?php } ?>>
                            <div class="animal-info-row">
                                <p><?= $animal->getName(); ?></p>
                                <p style="font-weight: 400; text-align: end;">
                                    <?php if ($animal -> getIsExactLocation() == "true" ) { ?>
                                        Exact marker
                                    <?php } else { ?>
                                        Indicative marker
                                    <?php } ?>
                                </p>
                            </div>
                            <div class="animal-info-row">
                                <p class="animal-description"><?= $animal->getLocationName(); ?></p>
                                <p style="font-weight: 400; text-align: end;"><?= date('M d, Y ', strtotime($animal->getCreatedAt())); ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </section>

        </div>

        <div hidden id="spinner"></div>

    </body>

<template id="animal-template">
    <div id="" class="animal-item">
        <img id="animal-image" src="">
        <div class="animal-description-preview"><p id="animal-description">desc</p></div>
        <div class="animal-item-content">
            <div class="animal-info-row">
                <p id="animal-name">name</p>
                <p id="animal-marker" style="font-weight: 400; text-align: end;">Indicative marker</p>
            </div>
            <div class="animal-info-row">
                <p id="animal-place" class="animal-description" style="font-weight: 400">Place</p>
                <p id="animal-date" style="font-weight: 400; text-align: end;">October 21, 2021</p>
            </div>
        </div>
    </div>
</template>

</html>
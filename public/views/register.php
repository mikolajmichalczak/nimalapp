<!DOCTYPE html>
<?php

    session_start();

?>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="public/css/style.css">

        <script type="text/javascript" src="./public/js/utils.js" defer></script>
        <script type="text/javascript" src="./public/js/register.js" defer></script>

        <title>Register</title>
    </head>

    <body>

        <div class="login-base-container">
            <img class="logo" style="cursor:pointer;" src="public/img/nimalLogo.svg">
            <div class="login-container" style="height: 80vh">
                <div class="login-label" style="height: 20%">
                    <h1>Register</h1>
                </div>
                <div class="messages" style="margin: 0;">
                    <?php
                    if(isset($messages)){
                        foreach($messages as $message) {
                            echo $message;
                        }
                    }
                    ?>
                </div>
                <form class="login-form" style="height: 70%" action="register" method="POST">
                    <input name="email" type="text" placeholder="email@email.com">
                    <input name="password" type="password" placeholder="password (min 8 characters)">
                    <input name="confirmedPassword" type="password" placeholder="confirm password">
                    <input name="name" type="text" placeholder="name">
                    <input name="surname" type="text" placeholder="surname (optional)">
                    <input name="phone" type="tel" placeholder="phone (optional)" pattern="[+]{1}[0-9]{11,14}">
                    <button id="login-button" class="submit-button" style="height: 14%;" type="submit" disabled>REGISTER</button>
                </form>
            </div>
        </div>

    </body>

</html>
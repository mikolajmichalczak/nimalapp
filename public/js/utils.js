let spinner = document.getElementById("spinner");

const months = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
]

function getFormattedDate(dateString){
    const date = new Date(dateString)
    const monthName = months[date.getMonth()]
    const day = ("0" + date.getDate().toString()).slice(-2)
    const year = date.getFullYear()

    return `${monthName} ${day}, ${year}`
}

function navigateToAnimalPreview() {

    spinner.removeAttribute('hidden');

    const id = this.getAttribute("id");

    fetch(`/previewAnimal/${id}`)
        .then(function (response) {
            return response.text();
        }).then(function (subpage) {
        spinner.setAttribute('hidden', '');
        document.write(subpage);
    });

}

function logout() {

    spinner.removeAttribute('hidden');

    fetch(`/logout`)
        .then(function (response) {
            if(response.ok){
                spinner.setAttribute('hidden', '');
                window.location.href = '/login';
            }
        });

}

function navigateToHomeScreen() { window.location.href = '/home'; }
function navigateToAnimalsList(){ window.location.href = '/animals'; }
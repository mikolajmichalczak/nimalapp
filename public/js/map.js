
mapboxgl.accessToken = 'pk.eyJ1IjoiZW1pa2VqIiwiYSI6ImNrd2VzcnZqeDA4MTUycG1vbTkyZ3drbGgifQ.HUE83Ch_LT98Lwav82NH3g';
const toggle = document.querySelector("input[type=checkbox]");

const map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/dark-v10',
    center: [19.921163, 49.272028],
    zoom: 12
});

document.getElementById("logout-button").addEventListener('click', function() {
    logout()
});

const geocoder = new MapboxGeocoder({
    accessToken: mapboxgl.accessToken,
    mapboxgl: mapboxgl,
    placeholder: 'Search for places'
});

map.addControl(
    new mapboxgl.GeolocateControl({
        positionOptions: {
            enableHighAccuracy: true
        },
        trackUserLocation: true,
        showUserHeading: true
    })
);

document.getElementById('geocoder').appendChild(geocoder.onAdd(map));

function navigateToAddAnimal(lat, lng, isExactLocation) {

    spinner.removeAttribute('hidden');

    fetch(`/createAnimal/?lat=${lat}&lng=${lng}&exact=${isExactLocation}`)
        .then(function (response) {
            return response.text();
        }).then(function (subpage) {
        spinner.setAttribute('hidden', '');
        document.write(subpage);
    });
}

document.getElementById("animals-list-button").addEventListener("click", navigateToAnimalsList)

function getUserAnimalsAndAddMarkers() {

    spinner.removeAttribute('hidden');

    fetch(`/getUserAnimals`)
        .then(function (response) {
            return response.json()
        }).then(function (animals){
            addMarkers(animals)
            spinner.setAttribute('hidden', '');
    })
}

function addMarkers(animals){
    animals.forEach( animal => {
        const el = document.createElement('div');
        el.className = animal.exact ? 'exact-marker' : 'indicative-marker';

        new mapboxgl.Marker(el)
            .setLngLat([animal.longitude, animal.latitude])
            .addTo(map);

        el.addEventListener("click", (e) => {
            new mapboxgl.Popup({ offset: 25 })
                .setHTML(
                    `    
                        <img class="marker-image" src="public/uploads/${animal.image}">
                        <h3>${animal.animal_name}</h3>
                        <button id = "${animal.animal_id}" class="show-animal-details">SHOW DETAILS</button>
                    `
                )
                .setLngLat([animal.longitude, animal.latitude])
                .addTo(map);

            const addExactMarkerButton = document.getElementById(animal.animal_id);
            addExactMarkerButton.addEventListener("click", navigateToAnimalPreview)

            e.stopPropagation();
        }, false);
    });
}

getUserAnimalsAndAddMarkers();

map.on('click', (e) => {

    const lat = e.lngLat.lat
    const lng = e.lngLat.lng

    new mapboxgl.Popup()
        .setLngLat(e.lngLat)
        .setHTML(
            `<button id="add-indicative-marker-button" class="add-marker-button">Add indicative marker</button>
        <button id="add-exact-marker-button" class="add-marker-button">Add exact marker</button>`
        )
        .setLngLat(e.lngLat)
        .addTo(map);

    const addIndicativeMarkerButton = document.getElementById("add-indicative-marker-button");
    const addExactMarkerButton = document.getElementById("add-exact-marker-button");
    addIndicativeMarkerButton.addEventListener("click", () => {
        navigateToAddAnimal(lat, lng, false)
    })
    addExactMarkerButton.addEventListener("click", () => {
        navigateToAddAnimal(lat, lng, true)
    })

});

toggle.addEventListener('change', function() {
    let indicativeMarkers = document.querySelectorAll(".indicative-marker");

    indicativeMarkers.forEach( marker => {
        marker.style.visibility = this.checked ? "visible" : "hidden"
    })

});

function showAnimalLocation() {
    const latLng = JSON.parse(sessionStorage.getItem("latLng"));
    if(latLng != null){
        sessionStorage.clear()
        map.flyTo({
            center: [
                latLng.longitude,
                latLng.latitude
            ],
            essential: true
        });
    }
}

showAnimalLocation()



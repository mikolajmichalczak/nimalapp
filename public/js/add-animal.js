const dropContainer = document.getElementById("drop-container")
const fileInput = document.getElementById("file-input")
const fileInputLabel = document.getElementById("file-input-label")
const logoContainer = document.getElementById("logo-container")
const closePreviewButton = document.getElementById("decline-button")

function navigateToHomeScreen() { window.location.href = '/home'; }

logoContainer.addEventListener("click", navigateToHomeScreen)
closePreviewButton.addEventListener("click", navigateToHomeScreen)

document.getElementById("logout-button").addEventListener('click', function() {
    logout()
});

dropContainer.ondragover = dropContainer.ondragenter = function(evt) {
    evt.preventDefault();
};
window.addEventListener("dragover",function(e){
    e = e || event;
    e.preventDefault();
},false);
window.addEventListener("drop",function(e){
    e = e || event;
    e.preventDefault();
},false);

dropContainer.ondrop = function(evt) {
    fileInput.files = evt.dataTransfer.files;

    const dT = new DataTransfer();
    dT.items.add(evt.dataTransfer.files[0]);
    fileInput.files = dT.files;

    evt.preventDefault();
    fileInputLabel.querySelector("p").innerHTML = fileInput.files[0].name;

};

fileInput.addEventListener('change', function () {
    if (this.value.length > 0) {
        fileInputLabel.querySelector("p").innerHTML = fileInput.files[0].name;
    }
});
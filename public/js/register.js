const form = document.querySelector("form");
const emailInput = form.querySelector('input[name="email"]');
const passwordInput = form.querySelector('input[name="password"]');
const nameInput = form.querySelector('input[name="name"]');
const confirmedPasswordInput = form.querySelector('input[name="confirmedPassword"]');
const registerButton = document.getElementById("login-button");
const logo = document.getElementsByClassName("logo");

logo[0].addEventListener("click", function () { navigateToHomeScreen() })

function isEmail(email) { return /\S+@\S+\.\S+/.test(email) }

function isValidPassword(password){ return password.length >= 8 }

function isValidName(name){ return name.length > 0 }

function arePasswordsSame(password, confirmedPassword) { return password === confirmedPassword }

function handleRegisterButton(){
    registerButton.disabled =
        !(isEmail(emailInput.value) &&
        arePasswordsSame(
            confirmedPasswordInput.previousElementSibling.value,
            confirmedPasswordInput.value
        ) &&
        isValidPassword(passwordInput.value) &&
        isValidName(nameInput.value))
}

function markValidation(element, condition) {
    !condition ? element.style.borderBottom = "2px solid red" : element.style.borderBottom = "1px solid #707070"
}

function validateEmail() {
    handleRegisterButton()
    setTimeout(function () {
            markValidation(emailInput, isEmail(emailInput.value));
        },
        1000
    );
}

function validatePasswords() {
    handleRegisterButton()
    setTimeout(function () {
            const condition = arePasswordsSame(
                confirmedPasswordInput.previousElementSibling.value,
                confirmedPasswordInput.value
            );
            markValidation(confirmedPasswordInput, condition);
        },
        1000
    );
}

function validatePassword(){
    handleRegisterButton()
    setTimeout(function () {
            markValidation(passwordInput, isValidPassword(passwordInput.value));
        },
        1000
    );
    validatePasswords()
}

function validateName(){
    handleRegisterButton()
    setTimeout(function () {
            markValidation(nameInput, isValidName(nameInput.value));
        },
        1000
    );
}

emailInput.addEventListener('keyup', validateEmail);
confirmedPasswordInput.addEventListener('keyup', validatePasswords);
passwordInput.addEventListener('keyup', validatePassword);
nameInput.addEventListener('keyup', validateName);
const search = document.querySelector('input[placeholder="Search for animals"]');
const animalContainer = document.querySelector(".animals");
const logoContainer = document.getElementById("logo-container")
const sortOptions = document.querySelectorAll(".animals-sort-option");
const toggle = document.getElementById("filter-markers-switch-container").querySelector("input[type=checkbox]");

logoContainer.addEventListener("click", () => { navigateToHomeScreen() })

function sortByAnimalNames( a, b ) {
    const firstAnimalName = a.animal_name.toLowerCase()
    const secondAnimalName = b.animal_name.toLowerCase()

    if ( firstAnimalName < secondAnimalName ){
        return -1;
    }
    if ( firstAnimalName > secondAnimalName ){
        return 1;
    }
    return 0;
}

function sortByAnimalDates( a, b ) {
    const firstAnimalDate = a.created_at
    const secondAnimalDate = b.created_at

    if ( firstAnimalDate > secondAnimalDate ){
        return -1;
    }
    if ( firstAnimalDate < secondAnimalDate ){
        return 1;
    }
    return 0;
}

function sortByMarkerAccuracy( a, b ) {
    const firstMarkerAccuracy = a.exact
    const secondMarkerAccuracy = b.exact

    if ( firstMarkerAccuracy > secondMarkerAccuracy ){
        return -1;
    }
    if ( firstMarkerAccuracy < secondMarkerAccuracy ){
        return 1;
    }
    return 0;
}

function sortAnimals(animals, sortOptionName) {
       switch (sortOptionName) {
       case "date-sort":
           return animals.sort(sortByAnimalDates);
       case "accuracy-sort":
           return animals.sort(sortByMarkerAccuracy);
       case "name-sort":
           return animals.sort(sortByAnimalNames);
           default:
               return animals
   }
}

function enableSortButtons(enable){
    toggle.disabled = !enable

    sortOptions.forEach( sortOption => {
        sortOption.style.pointerEvents = enable ? "auto" : "none"
    })
}

function searchAnimals(){

    spinner.removeAttribute('hidden');

    const sortOption = document.getElementsByClassName("animals-sort-option-selected")
    const sortOptionName = (sortOption.length === 0) ? "" : sortOption[0].id
    const areIndicativeMarkersVisible = toggle.checked

    const data = {
        search: search.value,
        indicative: areIndicativeMarkersVisible
    };

    fetch("/search", {
        method: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }).then(function (response) {
        return response.json();
    }).then(function (animals) {
        animalContainer.innerHTML = "";
        loadAnimals(sortAnimals(animals, sortOptionName))
        enableSortButtons(true)
        spinner.setAttribute('hidden', '');
    });
}

search.addEventListener("keyup", function (event) {
    if (event.key === "Enter") {
        event.preventDefault();
        searchAnimals()
    }
});

toggle.addEventListener('change', function() {
    enableSortButtons(false)
    searchAnimals()
});

function loadAnimals(animals) {
    animals.forEach(animal => {
        createAnimal(animal);
    });
}

function createAnimal(animal) {
    const template = document.querySelector("#animal-template");

    const clone = template.content.cloneNode(true);
    const div = clone.querySelector("div");
    div.id = animal.animal_id;
    const image = clone.querySelector("img");
    image.src = `/public/uploads/${animal.image}`;
    const name = clone.querySelector("#animal-name");
    name.innerHTML = animal.animal_name;
    const description = clone.querySelector("#animal-description");
    description.innerHTML = animal.description;
    const marker = clone.querySelector("#animal-marker");
    marker.innerHTML =  animal.exact ? "Exact marker" : "Indicative marker";
    const date = clone.querySelector("#animal-date");
    date.innerHTML = getFormattedDate(animal.created_at);
    const place = clone.querySelector("#animal-place");
    place.innerHTML = animal.location_name;

    if(!animal.exact){
        const animalDescPrev = clone.querySelector(".animal-description-preview");
        const animalItemContent = clone.querySelector(".animal-item-content");

        [animalItemContent, animalDescPrev].forEach( item => {
            item.style.backgroundColor= "#BFD3D3";
            item.style.color = "#26250F";
        })
    }

    animalContainer.appendChild(clone);
    addAnimalsClickListeners()
}

function selectOption(option){
    option.classList.add("animals-sort-option-selected")
    option.classList.remove("animals-sort-option")
}

function unselectOption(option){
    option.classList.remove("animals-sort-option-selected")
    option.classList.add("animals-sort-option")
}

function handleAnimalsSort(option){
    const isOptionSelected = option.classList.contains("animals-sort-option-selected")

    if(isOptionSelected){
        unselectOption(option)
    } else {
        sortOptions.forEach(option => {
            unselectOption(option)
        })
        selectOption(option)
    }
}

sortOptions.forEach(option => option.addEventListener("click", () => {
    enableSortButtons(false)
    handleAnimalsSort(option)
    searchAnimals()
}))

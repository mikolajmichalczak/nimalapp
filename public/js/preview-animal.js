const logoCont = document.getElementById("logo-container")
const closePreviewButton = document.getElementById("close-preview-button")
const showOnMapButton = document.getElementById("show-on-map-button")
const deleteButton = document.getElementById("delete-button")
spinner = document.getElementById("spinner");

logoCont.addEventListener("click", navigateToHomeScreen)

function handleBackNavigation(){
    switch (window.location.pathname) {
        case "/home":
            navigateToHomeScreen()
            break;
        case "/animals":
            navigateToAnimalsList()
            break;
    }
}

function showOnMap() {

    const animalId = showOnMapButton.querySelectorAll("div")[0].id

    spinner.removeAttribute('hidden');

    fetch(`/showOnMap/${animalId}`)
        .then(function (response) {
            return response.json()
        }).then(function (animal){
        const latLngJson = {
            latitude: animal.latitude,
            longitude: animal.longitude
        }
        sessionStorage.setItem("latLng", JSON.stringify(latLngJson));
        spinner.setAttribute('hidden', '');
        navigateToHomeScreen()
    });

}

function deleteAnimal(){

    const animalId = showOnMapButton.querySelectorAll("div")[0].id

    spinner.removeAttribute('hidden');

    fetch(`/deleteAnimal/${animalId}`)
        .then(function (response) {
            if(response.ok){
                handleBackNavigation()
                spinner.setAttribute('hidden', '');
            }
        })

}

showOnMapButton.addEventListener("click", showOnMap)

deleteButton.addEventListener("click", deleteAnimal)

closePreviewButton.addEventListener("click", () => {
    handleBackNavigation()
})

document.getElementById("logout-button").addEventListener('click', function() { logout() });



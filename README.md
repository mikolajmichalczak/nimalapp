 <h1>Link do repozytorium:</h1>
https://bitbucket.org/mikolajmichalczak/nimalapp/src/master/

<h1>Uruchamianie projektu:</h1>
W celu uruchomienia projektu należy sklonować repozytorium lub pobrać kod źródłowy.


Korzystając z narzędzia <b>Docker</b> oraz polecenia `docker-compose up` (wywołanego w głownym katalogu projektu) - aplikacja uruchomi się na lokalnym serwerze.


Aby móc z niej korzystać, w oknie przeglądarki należy wpisać: *http://localhost:8080*.

<h1>Opis projektu:</h1>
Aplikacja Nimal stworzona jest z myślą o miłośnikach przyrody oraz wędrówek. 

Pozwala na oznaczanie spotkanych zwierząt na mapie za pomocą pinów.


Istnieją dwa typy pinów:



<b>1. Piny dokładne</b>


<b>2. Piny orientacyjne </b>



Podczas dodawania zwierzęcia należy również załączyć jego zdjęcie, przez co aplikacja przyjmuje również charakter albumu i może stać się swego rodzaju portfolio


dla miłośników fotografii.

Projekt składa się z następujących ekranów:



<h3>1. Ekran logowania</h3>


Do zalogowania się niezbędny jest email oraz hasło.

<h3>2. Ekran rejestracji </h3>


Wymaganymi polami podczas rejestracji jest: email, hasło oraz imię użytkownika.

<h3>3. Ekran mapy</h3>


W ekranie tym dostępne są wszystkie piny - umieszczone na mapie, można decydować o widoczności pinów orientacyjnych, istnieje możliwość wyszukania lokacji oraz


śledzenia własnej lokalizacji.

<h3>4. Ekran listy zwierząt</h3>


Wszystkie dodane zwierzęta można wyświetlić w formie grida, sortować oraz wyszukiwać za pomocą searchBara.

<h3>5. Ekran dodawania zwierzęcia </h3>


Aby dodać zwierzę niezbędne jest jego zdjęcie oraz nazwa. Opcjonalnie można dodać opis, datę oraz nazwe lokacji. Dodać zdjęcie można przeciągając je i upuszczając lub


klikając przycisk i wybierając z odpowiedniego folderu.

<h3>6. Ekran podglądu zwierzęcia </h3>

<h1>Zrzuty ekranu aplikacji </h1>

![LoginWeb](https://user-images.githubusercontent.com/51120965/149993408-afe40238-eeee-4173-b500-69c0a0939c25.PNG)
![LoginMobile](https://user-images.githubusercontent.com/51120965/149997770-f8b45b20-355b-4552-9d9e-148265da34a3.PNG)
![MapWeb](https://user-images.githubusercontent.com/51120965/149997773-022391f6-1291-447e-b3b2-d5bf926ff0d6.PNG)
![MapMobile](https://user-images.githubusercontent.com/51120965/149997779-de6746eb-eeeb-4909-8a30-92999ef83511.PNG)
![MarkerMobile](https://user-images.githubusercontent.com/51120965/149997810-7bfbd04d-dbc4-4d97-b2e6-16ff5432b4cc.PNG)
![ListWeb](https://user-images.githubusercontent.com/51120965/149997814-b2ce955c-c9b6-4c58-8864-0a3901e9d961.PNG)
![ListMobile](https://user-images.githubusercontent.com/51120965/149997818-faf6a2a9-9ef7-465f-a9a3-acb9bd1bf8f3.PNG)
![PreviewAnimalWeb](https://user-images.githubusercontent.com/51120965/149997821-3269397a-8b76-491b-96a7-42536252bcad.PNG)
![PreviewAnimalMobile](https://user-images.githubusercontent.com/51120965/149997803-614c61f0-9b22-4652-bdd9-b03081c1f4bc.PNG)
![AddAnimalWeb](https://user-images.githubusercontent.com/51120965/149997807-4fde45e3-6193-4859-8af7-a67cdcb7720b.PNG)
![AddAnimalMobile](https://user-images.githubusercontent.com/51120965/149997809-6d8545c4-45cc-46e5-a905-a38b140ef944.PNG)



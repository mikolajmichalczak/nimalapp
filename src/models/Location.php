<?php

class Location
{
    private $latitude;
    private $longitude;
    private $exact;

    public function setLatitude($latitude): void
    {
        $this->latitude = $latitude;
    }

    public function setLongitude($longitude): void
    {
        $this->longitude = $longitude;
    }

    public function setExact($exact): void
    {
        $this->exact = $exact;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function getExact()
    {
        return $this->exact;
    }

    public function getName()
    {
        return $this->name;
    }
    private $name;

    public function __construct($latitude, $longitude, $exact, $name = null)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->exact = $exact;
        $this->name = $name;
    }
}
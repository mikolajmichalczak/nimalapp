<?php

class Animal {
    private $name;
    private $description;
    private $image;
    private $locationName;
    private $latitude;
    private $longitude;
    private $isExactLocation;
    private $createdAt;
    private $id;

    public function __construct($name, $description = null, $image = null, $locationName = null, $latitude, $longitude, $isExactLocation, $createdAt = null, $id = null)
    {
        $this->name = $name;
        $this->description = $description;
        $this->image = $image;
        $this->locationName = $locationName;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->isExactLocation = $isExactLocation;
        $this->createdAt = $createdAt;
        $this->id = $id;
    }


    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getLocationName()
    {
        return $this->locationName;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function getIsExactLocation()
    {
        return $this->isExactLocation;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

}
<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Animal.php';

class AnimalRepository extends Repository
{

    public function getAnimal(int $id, $isDataForFetch = false)
    {
        $stmt = $this->database->connect()->prepare('
            SELECT *, l.id as location_id, a.id as animal_id, a.name as animal_name, l.name as location_name FROM animals a LEFT JOIN locations l
            ON a.id_location = l.id WHERE a.id = :id;
        ');
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $animal = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($animal == false) {
            return null;
        }

        if($isDataForFetch){
            return $animal;
        }

        return new Animal(
            $animal['animal_name'],
            $animal['description'],
            $animal['image'],
            $animal['location_name'],
            $animal['latitude'],
            $animal['longitude'],
            $animal['exact'],
            $animal['created_at'],
            $animal['animal_id']
        );
    }

    /**
     * @throws Exception
     */
    public function addAnimal(Animal $animal, int $locationId): void{

        $date = new DateTime($animal -> getCreatedAt());

        $stmt = $this->database->connect()->prepare('
            INSERT INTO animals (id_user, name , description, created_at, image, id_location)
            VALUES (?, ?, ?, ?, ?, ?)
        ');

        if(isset($_SESSION['user_id'])) {
            $userId = $_SESSION['user_id'];

            $stmt->execute([
                $userId,
                $animal->getName(),
                $animal->getDescription(),
                $date->format('Y-m-d'),
                $animal->getImage(),
                $locationId
            ]);
        }
    }

    public function addLocation(Location $location): int{

        $dbConnection = $this->database->connect();
        $stmt = $dbConnection->prepare('
            INSERT INTO locations (name, latitude , longitude, exact)
            VALUES (?, ?, ?, ?)
        ');

        $stmt->execute([
            $location->getName(),
            $location->getLatitude(),
            $location->getLongitude(),
            $location->getExact()
        ]);

        return  $dbConnection->lastInsertId();
    }

    public function getAnimals(): array
    {
        $result = [];

        $stmt = $this->database->connect()->prepare('
            SELECT *, l.id as location_id, a.id as animal_id, a.name as animal_name, l.name as location_name FROM animals a LEFT JOIN locations l
            ON a.id_location = l.id;
        ');
        $stmt->execute();
        $animals = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($animals as $animal) {
            $result[] = new Animal(
                $animal['animal_name'],
                $animal['description'],
                $animal['image'],
                $animal['location_name'],
                $animal['latitude'],
                $animal['longitude'],
                $animal['exact'],
                $animal['created_at'],
                $animal['animal_id']
            );
        }

        return $result;
    }

    public function getUserAnimals($isDataForFetch = false): array
    {
        session_start();
        if(isset($_SESSION['user_id'])) {

            $result = [];
            $userId = $_SESSION['user_id'];

            $stmt = $this->database->connect()->prepare('
            SELECT *, l.id as location_id, a.id as animal_id, a.name as animal_name, l.name as location_name FROM animals a LEFT JOIN locations l
            ON a.id_location = l.id WHERE id_user = :userId;
        ');

            $stmt->bindParam(':userId', $userId);
            $stmt->execute();

            if($isDataForFetch){
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            }

            $animals = $stmt->fetchAll(PDO::FETCH_ASSOC);

            foreach ($animals as $animal) {
                $result[] = new Animal(
                    $animal['animal_name'],
                    $animal['description'],
                    $animal['image'],
                    $animal['location_name'],
                    $animal['latitude'],
                    $animal['longitude'],
                    $animal['exact'],
                    $animal['created_at'],
                    $animal['animal_id']
                );
            }

            return $result;
        }

        return [];
    }


    public function getAnimalByNameAndSort(string $searchString, string $areIndicativeMarkersVisible)
    {

        session_start();
        if(isset($_SESSION['user_id'])) {

            $userId = $_SESSION['user_id'];

            $searchString = '%' . strtolower($searchString) . '%';

            $query = $this -> getQuery($areIndicativeMarkersVisible);

            $stmt = $this->database->connect()->prepare($query);

            $stmt->bindParam(':search', $searchString);
            $stmt->bindParam(':userId', $userId);
            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }

        return [];
    }

    private function getQuery(string $areIndicativeMarkersVisible): string
    {
        if($areIndicativeMarkersVisible){
            return '
                 SELECT *, a.name as animal_name, l.name as location_name, a.id as animal_id, l.id as location_id FROM animals a LEFT JOIN locations l
                 ON a.id_location = l.id WHERE (LOWER(a.name) LIKE :search OR LOWER(description) LIKE :search) AND id_user = :userId;
            ';
        } else {
            return '
                 SELECT *, a.name as animal_name, l.name as location_name, a.id as animal_id, l.id as location_id FROM animals a LEFT JOIN locations l
                 ON a.id_location = l.id WHERE (LOWER(a.name) LIKE :search OR LOWER(description) LIKE :search) AND id_user = :userId AND l.exact = true;
            ';
        }
    }

    public function deleteAnimal(int $id)
    {
        $stmt = $this->database->connect()->prepare('
            DELETE FROM animals a WHERE a.id = :id;
        ');
        $stmt->bindParam(':id', $id);
        $stmt->execute();

    }
}
<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/Animal.php';
require_once __DIR__.'/../models/Location.php';
require_once __DIR__.'/../repository/AnimalRepository.php';

class AnimalController extends AppController {

    const MAX_FILE_SIZE = 1024*1024;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    const UPLOAD_DIRECTORY = '/../public/uploads/';

    private $message = [];
    private $animalRepository;

    public function __construct(){
        parent::__construct();
        $this -> animalRepository = new AnimalRepository();
    }

    public function home(){
        session_start();
        $animals = $this -> animalRepository -> getUserAnimals();
        $this->render('home', ["animals" => $animals]);
    }

    public function animals(){
        session_start();
        $animals = $this -> animalRepository -> getUserAnimals();
        $this->render('animals', ["animals" => $animals]);
    }

    public function getUserAnimals() {

        session_start();
        header('Content-type: application/json');
        http_response_code(200);
        echo json_encode($this->animalRepository->getUserAnimals(true));

    }

    public function addAnimal(){

        if ($this->isPost() && $this->validate($_FILES['file'], $_POST['name'])) {
            move_uploaded_file(
                $_FILES['file']['tmp_name'],
                dirname(__DIR__).self::UPLOAD_DIRECTORY.$_FILES['file']['name']
            );

            $location = new Location($_POST['latitude'], $_POST['longitude'], $_POST['exact'], $_POST['location']);

            $animalCreatedAt = $_POST["date"] ?: new DateTime();
            $animal = new Animal($_POST['name'], $_POST['description'], $_FILES['file']['name'], $_POST['location'], $_POST['latitude'], $_POST['longitude'], $_POST['exact'], $animalCreatedAt);

            session_start();
            $locationId = $this -> animalRepository -> addLocation($location);
            $this -> animalRepository -> addAnimal($animal, $locationId);

            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/home");
        }
        $this->render('add-animal', ['messages' => $this->message, 'latitude' => $_POST['latitude'], 'longitude' => $_POST['longitude'], 'isExactLocation' => $_POST['exact']]);
    }

    public function createAnimal($params)
    {
        http_response_code(200);
        $this->render(
            'add-animal',
            ["latitude" => $params["lat"], "longitude" => $params["lng"], "isExactLocation" => $params["exact"]]
        );
    }

    public function search()
    {
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

        if ($contentType === "application/json") {
            $content = trim(file_get_contents("php://input"));
            $decoded = json_decode($content, true);

            header('Content-type: application/json');
            http_response_code(200);

            echo json_encode($this->animalRepository->getAnimalByNameAndSort($decoded['search'],  $decoded['indicative']));
        }
    }

    private function validate(array $file, string $animalName): bool {

        $isValid = true;

        if(!is_uploaded_file($_FILES['file']['tmp_name'])){
            $this->message[] = 'Please upload animal photo.';
            $isValid = false;
        }

        if ($file['size'] > self::MAX_FILE_SIZE) {
            $this->message[] = 'File is too large for destination file system.';
            $isValid = false;
        }

        if (!isset($file['type']) || !in_array($file['type'], self::SUPPORTED_TYPES)) {
            $this->message[] = 'File type is not supported.';
            $isValid = false;
        }

        if(empty($animalName)){
            $this->message[] = 'Please insert animal name.';
            $isValid = false;
        }

        return $isValid;

    }

    public function previewAnimal($id)
    {
        $animal = $this->animalRepository->getAnimal($id);
        http_response_code(200);
        $this->render('animal-preview',  ["animal" => $animal]);
    }

    public function showOnMap($id)
    {
        session_start();
        header('Content-type: application/json');
        http_response_code(200);
        echo json_encode($this->animalRepository->getAnimal($id, true));
    }

    public function deleteAnimal($id)
    {
        $animal = $this->animalRepository->deleteAnimal($id);
        http_response_code(200);
    }

}
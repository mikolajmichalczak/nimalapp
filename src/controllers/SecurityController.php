<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../repository/UserRepository.php';

class SecurityController extends AppController {

    private UserRepository $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    public function login(){

        if (!$this->isPost()) {
            return $this->render('login');
        }

        $email = $_POST["email"];
        $password = $_POST["password"];

        if(empty($email) || empty($password)){
            return $this->render('login', ['messages' => ["Please fill all inputs!"]]);
        }

        $password = md5($_POST["password"]);

        $user = $this -> userRepository -> getUser($email);

        if(!$user){
            return $this->render('login', ['messages' => ["User not exist!"]]);
        }

        if($user -> getEmail() !== $email){
            return $this->render('login', ['messages' => ["User with this email not exist!"]]);
        }
        if($user -> getPassword() !== $password){
            return $this->render('login', ['messages' => ["Wrong password!"]]);
        }

        session_start();
        $userId = $this -> userRepository -> getUserId($user);
        $_SESSION['user_id'] = $userId;

        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/home");
    }

    public function register()
    {
        if (!$this->isPost()) {
            return $this->render('register');
        }

        $email = $_POST['email'];
        $password = $_POST['password'];
        $confirmedPassword = $_POST['confirmedPassword'];
        $name = $_POST['name'];
        $surname = $_POST['surname'];
        $phone = $_POST['phone'];

        $user = $this -> userRepository -> getUser($email);

        if($user){
            return $this->render('register', ['messages' => ["A user with this email address already exists!"]]);
        }

        if (empty($email) || empty($password) || empty($confirmedPassword) || empty($name)) {
            return $this->render('register', ['messages' => ['Please fill all inputs']]);
        }

        if ($password !== $confirmedPassword) {
            return $this->render('register', ['messages' => ['Please provide proper password']]);
        }

        $user = new User($email, md5($password), $name, $surname);
        $user->setPhone($phone);

        $this -> userRepository -> addUser($user);

        return $this->render('login', ['messages' => ['You\'ve been succesfully registrated!']]);
    }

    public function logout(){

        if(!isset($_SESSION)){ session_start(); }
        if(isset($_SESSION)){ session_destroy(); }

        http_response_code(200);

    }
}